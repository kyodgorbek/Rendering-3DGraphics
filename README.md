# Rendering-3DGraphics
Graphics g = getGraphics();
Graphics3D g3d = Graphics3D.getInstance();

 while(true) {
	 
	 g3d.bindTarget(g);
	 
	 // ...
	// Perform setup of the 3D scene to render.
 // ...
	 
	g3d.render(...);
	 
	 mGraphics3D.releaseTarget();
	 flushGraphics();
	 
	 
 }
